$(document).ready(function(){
	console.log("hi");
	$(".elem").click(function(){
		if (!$(this).hasClass("active")){
			$(".elem").removeClass("active");
			$(this).addClass("active");
		} else {
			$(this).removeClass("active");
		}
	})
	$(document).click(function(event){
		if ($(event.target).closest(".elem").length) return;
		if ($(".elem").hasClass("active")) {
			$(".elem").removeClass("active");
			$(".elem").next().fadeOut();
		}
	});
	
	$(".main_slider").owlCarousel({
		items: 1,
		nav: true,
		dots: true,
		loop: true,
		navSpeed: 1500,
		dotsSpeed: 1500,
		autoplaySpeed: 1500,
		autoplayTimeout: 5000,
		autoplayHoverPause: true,
		navText: ["", ""],
	});

	$(".header_button_mobile").click(function(){
		if (!$(this).hasClass("active")){
			$(this).addClass("active");
			$(this).next().addClass("active");
		} else {
			$(this).removeClass("active");
			$(this).next().removeClass("active");
		}
	})

	$(document).click(function(event){
		if ($(event.target).closest(".header_menu").length) return;
		$(".header_menu_list, .header_button_mobile").removeClass("active");
	});

	$(document).scroll(function(){
		if ($(document).scrollTop() > $(".header_top").innerHeight()) {
			$(".header_menu").addClass("fixed");
		} else {
			$(".header_menu").removeClass("fixed");
		}
	})

});