<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/result[@method = 'modules']/data">
		<xsl:apply-templates select="/result/@demo" mode="stopdoItInDemo" />
		<div class="tabs-content module">
		<div class="section selected">
		<div class="location" xmlns:umi="http://www.umi-cms.ru/TR/umi">
			<div class="saveSize"></div>
			<a class="btn-action loc-right infoblock-show">
				<i class="small-ico i-info"></i>
				<xsl:text>&help;</xsl:text>
			</a>
		</div>

		<div class="layout">
		<div class="column">
			<div class="row">
				<div class="col-md-6">
					<form action="{$lang-prefix}/admin/config/add_module_do/" enctype="multipart/form-data" method="post">
						<div class="field modules">
							<div>
								<div class="title-edit">
									<xsl:text>&label-install-path;</xsl:text>
								</div>
								<input value="classes/components/" class="default module-path" name="module_path"/>
								<input type="submit" class="btn color-blue {/result/@module}_{/result/@method}_btn install-btn"
									   value="&label-install;"/>
							</div>
						</div>
					</form>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<table class="btable btable-striped bold-head">
						<thead>
							<th>
								<xsl:text>&label-modules-list;</xsl:text>
							</th>

							<th>
								<xsl:text>&label-delete;</xsl:text>
							</th>
						</thead>

						<tbody>
							<xsl:apply-templates select="module" mode="list-view"/>
						</tbody>
					</table>
				</div>
			</div>

		</div>
			<div class="column">
				<div  class="infoblock">
					<h3>
						<xsl:text>&label-quick-help;</xsl:text>
					</h3>
					<div class="content" title="{$context-manul-url}">
					</div>
					<div class="infoblock-hide"></div>
				</div>
			</div>
		</div>
		</div>
		</div>
	</xsl:template>

	<xsl:template match="module" mode="list-view">
		<tr>
			<td>
				<a href="{$lang-prefix}/admin/{.}/">
					<xsl:value-of select="@label" />
				</a>
			</td>
			
			<td class="center">
				<a href="{$lang-prefix}/admin/config/del_module/{.}/" class="delete unrestorable {/result/@module}_{/result/@method}_btn" title="&label-delete;">
					<i class="small-ico i-remove"></i>
				</a>
			</td>
		</tr>
	</xsl:template>
</xsl:stylesheet>
