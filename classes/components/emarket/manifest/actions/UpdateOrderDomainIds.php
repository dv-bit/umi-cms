<?php
	namespace UmiCms\Manifest\Emarket;

	/** Команда обновления доменных идентификаторов у заказов на сайте */
	class UpdateOrderDomainIdsAction extends \Action {

		/** @inheritdoc */
		public function execute() {
			$orders = new \selector('objects');
			$orders->types('hierarchy-type')->name('emarket', 'order');
			$result = $orders->result();

			$currentDomain = \cmsController::getInstance()->getCurrentDomain();
			if (!$currentDomain instanceof \iDomain) {
				return;
			}

			/** @var \iUmiObject $order */
			foreach ($result as $order) {
				$order->setValue('domain_id', $currentDomain->getId());
				$order->commit();
			}
		}

		/**
		 * @inheritdoc
		 * @return $this
		 */
		public function rollback() {
			return $this;
		}
	}
