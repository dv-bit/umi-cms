<?php
	/**
	 * Способ оплаты через платежную систему "Robokassa"
	 */
	class roboxPayment extends payment {

		/**
		 * @var array $validTaxIdList список поддерживаемых идентификаторов ставок НДС
		 */
		private $validTaxIdList = [
			'none', 'vat0', 'vat10', 'vat18', 'vat110', 'vat118'
		];

		/**
		 * @const string REQUEST_URL адрес для запросов к Robokassa
		 */
		const REQUEST_URL = 'https://auth.robokassa.ru/Merchant/Index.aspx';

		/**
		 * @const int MAX_ORDER_ITEM_NAME_LENGTH максимальная длина названия товара (0 - первый символ)
		 */
		const MAX_ORDER_ITEM_NAME_LENGTH = 63;

		/**
		 * {@inheritdoc}
		 */
		public function validate() {
			return true;
		}

		/**
		 * {@inheritdoc}
		 */
		public static function getOrderId() {
			return (int) getRequest('shp_orderId');
		}

		/**
		 * {@inheritdoc}
		 * Устанавливает заказу статус оплаты "Инициализирована"
		 */
		public function process($template = null) {
			$object = $this->object;
			$login = (string) $object->getValue('login');
			$password = (string) $object->getValue('password1');
			$testMode = (int) $object->getValue('test_mode');

			if (!strlen($login) || !strlen($password)) {
				throw new publicException(getLabel('error-payment-wrong-settings'));
			}

			$order = $this->order;
			$orderPrice = number_format($order->getActualPrice(), 2, '.', '');
			$orderId = $order->getId();
			$needToSendReceipt = (bool) $object->getValue('receipt_data_send_enable');
			$receiptInfo = ($needToSendReceipt) ? $this->getReceiptInfo($order) : '';

			$signatureParts = [
				$login,
				$orderPrice,
				$orderId
			];

			if ($needToSendReceipt) {
				$signatureParts[] = $receiptInfo;
			}

			$signatureParts = array_merge($signatureParts, [
				$password,
				"shp_orderId=$orderId"
			]);

			$signature = $this->glueSignature($signatureParts);
			$languagePrefix = cmsController::getInstance()
				->getCurrentLang()
				->getPrefix();

			$templateData = [
				'formAction' => self::REQUEST_URL,
				'MrchLogin' => $login,
				'OutSum' => $orderPrice,
				'InvId' => $orderId,
				'Desc' => "Payment for order $orderId",
				'SignatureValue' => $signature,
				'IncCurrLabel' => '',
				'Culture' => strtolower($languagePrefix),
				'shp_orderId' => $orderId,
				'IsTest' => $testMode
			];

			if ($needToSendReceipt) {
				$templateData['Receipt'] = $receiptInfo;
			}

			$order->order();
			$order->setPaymentStatus('initialized');

			list($templateString) = emarket::loadTemplates(
				'emarket/payment/robokassa/' . $template,
				'form_block'
			);

			return emarket::parseTemplate($templateString, $templateData);
		}

		/**
		 * Валидирует заказ платежной системы.
		 * Если заказа валиден - заказу в UMI.CMS
		 * устанавливается номер платежного документа
		 * и статус оплаты "Принята".
		 * @throws coreException
		 */
		public function poll() {
			$orderPrice  = (string) getRequest('OutSum');
			$invoiceId = (string) getRequest('InvId');
			$requestSignature = (string) getRequest('SignatureValue');
			$orderId = (string) getRequest('shp_orderId');

			$object = $this->object;
			$password = (string) $object->getValue('password2');

			$signature = $this->glueSignature([
				$orderPrice,
				$invoiceId,
				$password,
				"shp_orderId=$orderId"
			]);

			$isCorrectSignature = strcasecmp($requestSignature, $signature) == 0;

			$order = $this->order;
			$isCorrectOrderPrice = (float) $order->getActualPrice() == (float) $orderPrice;

			/**
			 * @var HTTPDocOutputBuffer $buffer
			 */
			$buffer = outputBuffer::current();
			$buffer->clear();
			$buffer->contentType('text/plain');

			if (!$isCorrectSignature || !$isCorrectOrderPrice) {
				$buffer->push("failed");
				$buffer->end();
			}

			$order->setPaymentStatus('accepted');
			$order->payment_document_num = $invoiceId;
			$order->commit();

			$buffer->push("OK{$invoiceId}");
			$buffer->end();
		}

		/**
		 * Склеивает подпись (контрольный параметр)
		 * @param string[] $parts части подписи
		 * @return string
		 */
		private function glueSignature(array $parts) {
			$signature = implode(':', $parts);
			return md5($signature);
		}

		/**
		 * Возвращает данные для печати чека.
		 * @link https://docs.robokassa.ru/#6865
		 * @param order $order $order заказ
		 * @return string json
		 * @see roboxPayment::getOrderItemInfoList()
		 * @throws publicException
		 */
		private function getReceiptInfo(order $order) {
			$receiptInfo = $this->getOrderItemInfoList($order);

			if (!is_array($receiptInfo)) {
				throw new publicException(getLabel('error-payment-wrong-receipt-info'));
			}

			return urlencode(json_encode($receiptInfo));
		}

		/**
		 * Возвращает информацию о списке товарных наименований
		 * @param order $order заказ
		 * @return array
		 *
		 * [
		 *      'items' =>  [
		 *          # => @see roboxPayment::getOrderItemInfo()
		 *      ]
		 * ]
		 *
		 * @throws publicException
		 */
		private function getOrderItemInfoList(order $order) {
			$orderItemList = $order->getItems();

			if (!is_array($orderItemList) || empty($orderItemList)) {
				throw new publicException(getLabel('error-payment-empty-order'));
			}

			$orderItemDataList = [];

			foreach ($orderItemList as $orderItem) {
				$orderItemDataList[] = $this->getOrderItemInfo($orderItem);
			}

			try {
				$deliveryInfo = $this->getDeliveryInfo($order);
				$orderItemDataList[] = $deliveryInfo;
			} catch (expectObjectException $e) {
				//nothing
			}

			return [
				'items' => $orderItemDataList
			];
		}

		/**
		 * Возвращает информацию о товарном наименовании
		 * @param orderItem $orderItem товарное наименование
		 * @return array
		 *
		 * [
		 *      'name' => Название товара,
		 *      'quantity' => Количество товара,
		 *      'sum' => Цена за единицу товара,
		 *      'tax' => id ставки НДС
		 * ]
		 */
		private function getOrderItemInfo(orderItem $orderItem) {
			return [
				'name' => substr($orderItem->getName(), 0, self::MAX_ORDER_ITEM_NAME_LENGTH),
				'quantity' => sprintf('%.3f', $orderItem->getAmount()),
				'sum' => sprintf('%.2f', $orderItem->getActualPrice()),
				'tax' => $this->getEntityTaxRateId($orderItem->getObject())
			];
		}

		/**
		 * Возвращает информацию о доставке заказа
		 * @param order $order заказ
		 * @return array
		 * @see roboxPayment::getOrderItemInfo()
		 * @throws expectObjectException
		 */
		private function getDeliveryInfo(order $order) {
			$delivery = selector::get('object')->id(
				$order->getValue('delivery_id')
			);

			if (!$delivery instanceof iUmiObject) {
				throw new expectObjectException(getLabel('error-unexpected-exception'));
			}

			return [
				'name' => substr($delivery->getName(), 0, self::MAX_ORDER_ITEM_NAME_LENGTH),
				'quantity' => sprintf('%.3f', 1),
				'sum' => sprintf('%.2f', $order->getValue('delivery_price')),
				'tax' => $this->getEntityTaxRateId($delivery)
			];
		}

		/**
		 * Возвращает идентификатор ставки НДС
		 * @param iUmiObject $object сущность со ставкой (способ доставки или наименование заказа)
		 * @return int
		 * @throws publicException
		 */
		private function getEntityTaxRateId(iUmiObject $object) {
			$taxRate = selector::get('object')
				->id($object->getValue('tax_rate_id'));

			if (!$taxRate instanceof iUmiObject) {
				throw new publicException(getLabel('error-payment-order-item-empty-tax'));
			}

			$taxRateId = (string) $taxRate->getValue('robokassa_id');

			if (!in_array($taxRateId, $this->validTaxIdList)) {
				throw new publicException(getLabel('error-payment-order-item-empty-tax'));
			}

			return $taxRateId;
		}
	}