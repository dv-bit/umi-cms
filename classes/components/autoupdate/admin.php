<?php
	/**
	 * Класс функционала административной панели
	 */
	class AutoupdateAdmin{

		use baseModuleAdmin;
		/**
		 * @var autoupdate $module
		 */
		public $module;

		/**
		 * Возвращает информацию о состоянии обновлений системы
		 * @throws coreException
		 */
		public function versions() {
			$umiRegistry = regedit::getInstance();
			$systemEdition = $umiRegistry->get("//modules/autoupdate/system_edition");
			$systemEditionStatus = "%autoupdate_edition_" . $systemEdition . "%";

			if (
				$systemEdition == "commerce_trial" &&
				$_SERVER['HTTP_HOST'] != 'localhost' &&
				$_SERVER['HTTP_HOST'] != 'subdomain.localhost' &&
				$_SERVER['SERVER_ADDR'] != '127.0.0.1'
			) {
				$daysLeft = $umiRegistry->getDaysLeft();
				$systemEditionStatus .= " ({$daysLeft} " . getLabel('label-days-left') . ")";
			}

			$systemEditionStatus = autoupdate::parseTPLMacroses($systemEditionStatus);
			$lastUpdateTime = $umiRegistry->get("//modules/autoupdate/last_updated");

			$params = [
				"autoupdate" => [
					"status:system-edition"	=> $systemEditionStatus,
					"status:last-updated" => date("Y-m-d H:i:s", $lastUpdateTime),
					"status:system-version" => $umiRegistry->get("//modules/autoupdate/system_version"),
					"status:system-build" => $umiRegistry->get("//modules/autoupdate/system_build"),
					"status:db-driver" => iConfiguration::MYSQL_DB_DRIVER,
					"boolean:disabled" => false
				]
			];

			if (defined("CURRENT_VERSION_LINE")) {
				$isStartEdition =  in_array(CURRENT_VERSION_LINE, array("start"));

				if (isDemoMode() || $isStartEdition) {
					$params['autoupdate']['boolean:disabled'] = true;
				}
			}

			$domainsCollection = domainsCollection::getInstance();

			if (!$domainsCollection->isDefaultDomain()) {
				$params['autoupdate']['check:disabled-by-host'] = $domainsCollection->getDefaultDomain()->getHost();
			}

			$this->setDataType("settings");
			$this->setActionType("view");
			$data = $this->prepareData($params, "settings");
			$this->setData($data);
			$this->doData();
		}
	}
?>
