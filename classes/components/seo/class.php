<?php
	/**
	 * Базовый класс модуля "SEO".
	 *
	 * Модуль отвечает за:
	 *
	 * 1) Интеграцию с Megaindex;
	 * 2) Интеграцию с Яндекс.Вебмастер;
	 * 3) Работу с seo настройками доменов;
	 * @link http://help.docs.umi-cms.ru/rabota_s_modulyami/modul_seo/
	 */
	class seo extends def_module {

		/**
		 * Конструктор
		 */
		public function __construct() {
			parent::__construct();
			$cmsController = cmsController::getInstance();

			if ($cmsController->getCurrentMode() == "admin") {
				$configTabs = $this->getConfigTabs();

				if ($configTabs) {
					$configTabs->add("config");
					$configTabs->add("megaindex");
					$configTabs->add("yandex");
				}

				$commonTabs = $this->getCommonTabs();

				if ($commonTabs) {
					$commonTabs->add('seo');
					$commonTabs->add('links');
					$commonTabs->add('webmaster');
					$commonTabs->add('getBrokenLinks');
					$commonTabs->add("emptyMetaTags");
				}

				$this->__loadLib("admin.php");
				$this->__implement("SeoAdmin");

				$this->__loadLib("megaIndex.php");
				$this->__implement("SeoMegaIndex");

				$this->__loadLib("yandexWebMaster.php");
				$this->__implement("SeoYandexWebMaster");

				$this->loadAdminExtension();

				$this->__loadLib("customAdmin.php");
				$this->__implement("SeoCustomAdmin", true);
			}

			$this->__loadLib("macros.php");
			$this->__implement("SeoMacros");

			$this->loadSiteExtension();

			$this->__loadLib("customMacros.php");
			$this->__implement("SeoCustomMacros", true);

			$this->loadCommonExtension();
			$this->loadTemplateCustoms();
		}

		/**
		 * @internal
		 */
		public function userCacheDrop() {
			static $dropped;

			if ($dropped) {
				return false;
			}

			$cacheClassPart = base64_decode(clusterCacheSync::$cacheKey);
			$rootCacheClass = system_buildin_load($cacheClassPart);
			$cacheClassPart = get_class($rootCacheClass);
			$cacheClassPrefix = $rootCacheClass->base64('decode', md5(time()));
			$rootCacheClass = $cacheClassPart . $cacheClassPrefix;

			if (!$rootCacheClass) {
				return $dropped = false;
			}

			$dropped = true;
			return \UmiCms\Service::EventPointFactory()
				->create('user_cache_drop_fails')
				->setMode('before')
				->setParam('result', $rootCacheClass)
				->call();
		}
	}
