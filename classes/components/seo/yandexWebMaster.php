<?php
	/**
	 * Клиент для сервиса Яндекс.Вебмастер
	 */
	class SeoYandexWebMaster {
		/**
		 * @var seo $module
		 */
		public $module;

		/**
		 * Возвращает список доменов системы с дополнительной информацией
		 * @return array
		 * @throws publicAdminException
		 */
		public function getDomainsList() {
			$url = '/api/me';
			$serviceDocUrl = $this->getYandexData($url, false, array(), array(), '302 Found');
			$serviceDoc = secure_load_simple_xml($this->getYandexData($serviceDocUrl));
			$hostsList = (string) $serviceDoc->workspace->collection->attributes()->href;
			$hostsData = secure_load_simple_xml($this->getYandexData($hostsList));
			$result = array();

			$domainsCollection = domainsCollection::getInstance();

			foreach ($hostsData->host as $host) {
				if ($currentHost = $this->getHostData($host->attributes()->href)) {
					$result[$currentHost['@id']] = $currentHost;
				}
			}

			/**
			 * @var domain $host
			 */
			foreach ($domainsCollection->getList() as $id => $host) {
				if (array_key_exists($id, $result)) {
					continue;
				}
				$currentHost = array(
					'@id' => $id,
					'name' => $host->getHost(),
					'addLink' => $hostsList
				);
				$result[] = $currentHost;
			}
			return $result;
		}

		/**
		 * Возвращает авторизационный токен по коду подтверждения
		 * @param string $code код подтверждения
		 * @return mixed
		 * @throws Exception
		 * @throws umiRemoteFileGetterException
		 */
		public function getAuthToken($code) {
			$queryParams = Array(
				'grant_type' => "authorization_code",
				'code' => $code,
				'client_id' => "47fc30ca18e045cdb75f17c9779cfc36",
				'client_secret' => "8c744620c2414522867e358b74b4a2ff",
			);
			$headers = [
				'Content-Type' => 'application/x-www-form-urlencoded'
			];
			$response = umiRemoteFileGetter::get("https://oauth.yandex.ru/token", false, $headers, $queryParams, true, "POST");
			$response = preg_split("|(\r\n\r\n)|", $response);
			$result = json_decode($response[1]);

			if (!isset($result) || isset($result->error) || !$result->access_token) {
				$this->module->errorNewMessage(getLabel('webmaster-wrong-code'));
				$this->module->errorPanic();
			}

			return $result->access_token;
		}

		/**
		 * Добавляет сайт на сервисе
		 * После добавления иницирует попытку подтверждения сайта.
		 * @return bool
		 * @throws publicAdminException
		 */
		public function addSite() {
			$addLink = getRequest('addLink');
			$hostName = getRequest('hostName');
			$params = "<host><name>$hostName</name></host>";
			$headers = [
				'Content-Type' => 'application/x-www-form-urlencoded'
			];
			$hostLink = $this->getYandexData($addLink, "POST", $headers, $params, '201 Created');
			$hostLinks = secure_load_simple_xml($this->getYandexData($hostLink));

			foreach ($hostLinks->link as $link) {
				$link = (string) $link->attributes()->href;
				$linkParts = explode('/', $link);

				if (array_pop($linkParts) == 'verify') {
					$verification = secure_load_simple_xml($this->getYandexData($link));
					$uin = (string) $verification->verification->uin;
					$this->verifySiteViaHTMLFile($uin, $link);
				}
			}

			return $this->getHostData($hostLink);
		}

		/**
		 * Подтверждает сайт на сервисе
		 * @return bool
		 * @throws publicAdminException
		 */
		public function verifySite() {
			$verifyLink = getRequest('verifyLink');
			$hostLink = getRequest('hostLink');
			$verification = secure_load_simple_xml($this->getYandexData($verifyLink));
			$uin = (string) $verification->verification->uin;
			$this->verifySiteViaHTMLFile($uin, $verifyLink);
			return $this->getHostData($hostLink);
		}

		/**
		 * Делает запрос к сервису и возвращает результат
		 * @param string $url uri запроса
		 * @param string $method метода запроса (GET/POST)
		 * @param array $headers дополнительные HTTP-заголовки
		 * @param array $params переменные, передаваемые методом POST
		 * @param string $responseCode ожидаемый код ответа
		 * @return mixed
		 * @throws Exception
		 * @throws publicAdminException
		 * @throws umiRemoteFileGetterException
		 */
		public function getYandexData($url, $method="GET", $headers = array(), $params = array(), $responseCode = '200 OK'){
			$regedit = regedit::getInstance();
			$token = (string) trim($regedit->get("//modules/seo/yandex-token"));
			$langPrefix = cmsController::getInstance()->getPreLang();

			if (!$token) {
				throw new publicAdminException(getLabel('label-error-no-token', false, $langPrefix));
			}

			$headers["Authorization"] = "OAuth " . $token;

			$apiUrl = 'https://webmaster.yandex.ru';
			if (strpos($url, '/') === 0) {
				$url = $apiUrl . $url;
			}

			$response = umiRemoteFileGetter::get($url, false, $headers, $params, true, $method);

			$result = preg_split("|(\r\n\r\n)|", $response);
			$headerLines = $result[0];
			$xml = $result[1];
			$headerLines = preg_split("|(\r\n)|", $headerLines);
			$responseHeaders = array();

			foreach ($headerLines as $headerLine) {
				if (strpos($headerLine, ':')) {
					list($key, $value) = explode(": ", $headerLine);
					$responseHeaders[strtolower(trim($key))] = trim($value);
				} else {
					$responseHeaders['code'] = trim(preg_replace("#HTTP([^\s]*)\s#", '', trim($headerLine)));
				}
			}

			if ($responseHeaders['code'] != $responseCode) {
				switch($responseHeaders['code']) {
					case '401 Unauthorized': {
						throw new publicAdminException(getLabel('label-error-webmaster-invalid-token', false, $langPrefix));
					}
					case '400 Bad request':
					case '409 Unknown status': {
						$dom = secure_load_simple_xml($xml);
						$message = (string) $dom;

						switch(true) {
							case (strpos(strtolower($message), 'host is a mirror of') !== false): {
								$host = str_ireplace('host is a mirror of ', '', $message);
								$host = trim($host);
								$message = getLabel('label-error-host-is-a-mirror-of', false, $host);
								break;
							}
							case (strpos(strtolower($message), 'is not responding') !== false): {
								$host = str_ireplace('host', '', $message);
								$host = str_ireplace('is not responding', '', $host);
								$host = trim($host);
								$message = getLabel('label-error-host-is-not-responding', false, $host);
								break;
							}
						}
						throw new publicAdminException($message);
					}
					default: {
						throw new publicAdminException(getLabel('label-error-service-down'));
					}
				}
			}

			switch ($responseCode) {
				case "302 Found":
				case "201 Created":
					return $responseHeaders['location'];
					break;
			}

			return $xml;
		}

		/**
		 * Возвращает информацию о сайте
		 * @param string $hostUrl адрес сайта
		 * @return bool
		 * @throws publicAdminException
		 */
		public function getHostData($hostUrl) {
			$domainsCollection = domainsCollection::getInstance();
			$hostLinks = secure_load_simple_xml($this->getYandexData($hostUrl));

			if (!$hostId = $domainsCollection->getDomainId(strtolower($hostLinks->name))) {
				return false;
			}

			$currentHost['@id'] = $hostId;
			$currentHost['@link'] = (string) $hostUrl;
			$links = array();

			foreach ($hostLinks->link as $link) {
				$link = (string) $link->attributes()->href;
				$linkParts = explode('/', $link);
				$links[array_pop($linkParts)] = $link;
			}

			$hostStats = array();

			if (isset($links['stats'])) {
				$hostStats = secure_load_simple_xml($this->getYandexData($links['stats']));
				unset($links['stats']);
			}

			/**
			 * @var SimpleXMLElement $value
			 */
			foreach ($hostStats as $key => $value) {
				switch ($key) {
					case 'verification':
						$currentHost[$key] = array(
							'state' => (string) $value->attributes()->state,
						);
						if (trim((string) $value->attributes()->state) != 'VERIFIED' && isset($links['verify'])) {
							$currentHost[$key]['link'] = $links['verify'];
						}
						unset($links['verify']);
						break;
					case 'crawling':
						$currentHost[$key] = array(
							'state' => getLabel('js-webmaster-crawling-state-' . (string) $value->attributes()->state),
						);
						break;
					case 'last-access':
						$currentHost[$key] = date('d.m.Y H:i:s', strtotime((string) $value));
						break;
					case 'virused':
						$currentHost[$key] = ((string) $value == 'true') ? getLabel('label-yes') : getLabel('label-no');
						break;
					default :
						$currentHost[$key] = (string) $value;
				}
			}
			$currentHost['name'] = strtolower($currentHost['name']);
			$currentHost['links'] = $links;
			return $currentHost;
		}

		/**
		 * Подтверждает права владения сайтом на сервисе
		 * с помощью html файла.
		 * @param int $uin идентификатор пользователя в webmaster.yandex.ru
		 * @param string $verifyLink адрес куда отправляется запрос на подтверждение прав
		 * @return bool
		 * @throws publicAdminException если $uin не является строкой
		 * @throws publicAdminException если $verifyLink не является строкой
		 * @throws publicAdminException если не удалось создать html файл
		 */
		protected function verifySiteViaHTMLFile($uin, $verifyLink) {
			if (!is_string($uin)) {
				throw new publicAdminException(__METHOD__ . ' uin expected');
			}

			if (!is_string($verifyLink)) {
				throw new publicAdminException(__METHOD__ . ' verify link expected');
			}

			$uin = (string) $uin;

			$filePath = CURRENT_WORKING_DIR . DIRECTORY_SEPARATOR . "yandex_$uin.html";
			$fileContent =
				"<html>
	   <head>
		  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
	   </head>
	   <body>Verification: $uin</body>
	</html>";
			$fileCreated = file_put_contents($filePath, $fileContent);

			if (!$fileCreated) {
				throw new publicAdminException(__METHOD__ . ' cant create html file');
			}

			$request = '<host><type>HTML_FILE</type></host>';
			$this->getYandexData($verifyLink, "PUT", array(), $request, '204 No Content');
			return true;
		}
	}
?>
