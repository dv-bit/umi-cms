<?php
	/**
	 * Клиент для сервиса MegaIndex
	 */
	class SeoMegaIndex{
		/**
		 * @var seo $module
		 */
		public $module;

		/**
		 * Возвращает видимость сайта / метод siteAnalyze
		 * @see http://api.megaindex.ru/description/siteAnalyze
		 *
		 * @param string $login логин в MegaIndex
		 * @param string $password пароль в MegaIndex
		 * @param string $site исследуемый сайт в MegaIndex
		 * @param string $date дата, за которую возвращать статистику
		 *
		 * @return mixed
		 * @throws coreException
		 */
		public function siteAnalyzeJson($login, $password, $site, $date) {
			$array = array(
				'login' => $login,
				'password' => $password,
				'url' => $site,
				'date' => $date
			);

			try {
				$content = umiRemoteFileGetter::get("http://api.megaindex.ru/?method=siteAnalyze&" . http_build_query($array));
			} catch (umiRemoteFileGetterException $e) {
				$content = false;
			}

			$json = json_decode($content);

			if (!is_object($json)) {
				throw new coreException(getLabel('error-data'));
			}

			if ($json->status != 0) {
				$message = (isset($json->error)) ? getLabel('error') . $json->error : getLabel('error');
				throw new coreException($message);
			}

			return $json;
		}

		/**
		 * Возвращает ссылки на сайт / метод get_backlinks
		 * @see http://api.megaindex.ru/description/get_backlinks
		 *
		 * @param string $login логин в MegaIndex
		 * @param string $password пароль в MegaIndex
		 * @param string $site исследуемый сайт в MegaIndex
		 *
		 * @return mixed
		 * @throws coreException
		 */
		public function getBackLinks($login, $password, $site) {
			$params = array(
				'login' => $login,
				'password' => $password,
				'url' => $site,
				'method' => 'get_backlinks',
				'output' => 'json'
			);

			$headers = array(
				"Content-type" => "application/x-www-form-urlencoded"
			);

			$response = umiRemoteFileGetter::get('http://api.megaindex.ru/?' . http_build_query($params), false, $headers);
			return json_decode($response);
		}
	}
?>