<?php

	/**
	 * Управляет правами доступа на страницы и ресурсы модулей.
	 * Синглтон. Экземпляр класса можно получить через статический метод getInstance.
	 */
	class permissionsCollection extends singleton implements iSingleton, iPermissionsCollection {
		protected $methodsPermissions = [], $tempElementPermissions = [];
		protected $elementsCache = [];
		private $umiTypesHelper;
		private $umiPropertiesHelper;

		// Some permissions constants
		const E_READ_ALLOWED = 0;
		const E_EDIT_ALLOWED = 1;
		const E_CREATE_ALLOWED = 2;
		const E_DELETE_ALLOWED = 3;
		const E_MOVE_ALLOWED = 4;

		const E_READ_ALLOWED_BIT = 1;
		const E_EDIT_ALLOWED_BIT = 2;
		const E_CREATE_ALLOWED_BIT = 4;
		const E_DELETE_ALLOWED_BIT = 8;
		const E_MOVE_ALLOWED_BIT = 16;

		/**
		 * Алгоритм хеширования пароля SHA256
		 */
		const SHA256 = 0;

		/**
		 * Алгоритм хеширования пароля md5
		 */
		const MD5 = 1;

		/**
		 * Соль для хеширования пароля
		 */
		const HASH_SALT = "o95j43hiwjrthpoiwj45ihwpriobneop;jfgp3408ghqpqh5gpqoi4hgp9q85h";

		/**
		 * Конструктор
		 */
		public function __construct() {
			$this->umiTypesHelper = umiTypesHelper::getInstance();
			$this->umiPropertiesHelper = umiPropertiesHelper::getInstance();
		}

		/**
		 * Получить экземпляр коллекци
		 * @return permissionsCollection экземпляр класса permissionsCollection
		 */
		public static function getInstance($c = null) {
			return parent::getInstance(__CLASS__);
		}

		/**
		 * Возвращает список групп пользователя или его идентификатор, если группы не заданы
		 * @param int $ownerId id пользователя или группы
		 * @return Integer|array
		 */
		public function getOwnerType($ownerId) {
			static $cache = [];

			$ownerId = intval($ownerId);

			if (isset($cache[$ownerId])) {
				return $cache[$ownerId];
			}

			$userTypeId = $this->umiTypesHelper->getObjectTypeIdByGuid('users-user');

			$groups = $this->umiPropertiesHelper->getProperty(
					$ownerId,
					'groups',
					$userTypeId
			);

			if ($groups instanceof umiObjectProperty) {
				$cache[$ownerId] = $groups->getValue();
				return $cache[$ownerId];
			}

			return $ownerId;
		}

		/**
		 * Внутрисистемный метод, не является частью публичного API
		 * @param Integer $owner_id id пользователя или группы
		 * @return String фрагмент SQL-запроса
		 */
		public function makeSqlWhere($owner_id, $ignoreSelf = false) {
			static $cache = [];

			if (isset($cache[$owner_id])) {
				return $cache[$owner_id];
			}

			$owner = $this->getOwnerType($owner_id);

			if (is_numeric($owner)) {
				$owner = [];
			}

			if ($owner_id) {
				$owner[] = $owner_id;
			}

			$owner[] = UmiCms\Service::SystemUsersPermissions()->getGuestUserId();
			$owner = array_unique($owner);

			if (sizeof($owner) > 2) {
				foreach ($owner as $i => $id) {
					if ($id == $owner_id && $ignoreSelf) {
						unset($owner[$i]);
					}
				}
				$owner = array_unique($owner);
				sort($owner);
			}

			$sql = "";
			$sz = sizeof($owner);

			for ($i = 0; $i < $sz; $i++) {
				$sql .= "cp.owner_id = '{$owner[$i]}'";
				if ($i < ($sz - 1)) {
					$sql .= " OR ";
				}
			}

			return $cache[$owner_id] = "({$sql})";
		}

		/**
		 * Узнать, разрешен ли пользователю или группе $owner_id доступ к модулю $module
		 * @param Integer $owner_id id пользователя или группы пользователей
		 * @param String $module название модуля
		 * @return Boolean true если доступ разрешен
		 */
		public function isAllowedModule($owner_id, $module) {
			static $cache = [];

			if ($owner_id == false) {
				$owner_id = UmiCms\Service::Auth()->getUserId();
			}

			if ($this->isSv($owner_id)) {
				return true;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$module = $connection->escape($module);

			if (substr($module, 0, 7) == "macros_") {
				return false;
			}

			if (isset($cache[$owner_id][$module])) {
				return $cache[$owner_id][$module];
			}

			$sql_where = $this->makeSqlWhere($owner_id);
			$sql = "SELECT module, MAX(cp.allow) FROM cms_permissions cp WHERE method IS NULL AND {$sql_where} GROUP BY module";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			foreach ($result as $row) {
				list($m, $allow) = $row;
				$cache[$owner_id][$m] = $allow;
			}

			return isset($cache[$owner_id][$module]) ? (bool) $cache[$owner_id][$module] : false;
		}

		/**
		 * Узнать, разрешен ли пользователю или группе $owner_id доступ к методу $method модуля $module
		 * @param Integer $owner_id id пользователя или группы пользователей
		 * @param String $module название модуля
		 * @param String $method название метода
		 * @return Boolean true если доступ на метод разрешен
		 */
		public function isAllowedMethod($owner_id, $module, $method, $ignoreSelf = false) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$module = $connection->escape($module);
			$owner_id = (int) $owner_id;

			if ($module == "content" && !strlen($method)) {
				return 1;
			}

			if ($module == "config" && $method == "menu") {
				return 1;
			}

			if ($module == "eshop" && $method == "makeRealDivide") {
				return 1;
			}

			if ($this->isAdmin($owner_id) && $this->isAdminAllowedMethod($module, $method)) {
				return 1;
			}

			if ($this->isSv($owner_id)) {
				return true;
			}

			if (!$module) {
				return false;
			}

			$method = $this->getBaseMethodName($module, $method);

			if ($module == "backup" && $method == "rollback") {
				return true;
			}

			if ($module == "autoupdate" && $method == "service") {
				return true;
			}

			if ($module == "config" && ($method == "lang_list" || $method == "lang_phrases")) {
				return true;
			}

			if ($module == "users" && ($method == "auth" || $method == "login_do" || $method == "login")) {
				return true;
			}

			$methodsPermissions = &$this->methodsPermissions;

			if (!isset($methodsPermissions[$owner_id]) || !is_array($methodsPermissions[$owner_id])) {
				$methodsPermissions[$owner_id] = [];
			}

			$cache = &$methodsPermissions[$owner_id];

			$sql_where = $this->makeSqlWhere($owner_id, $ignoreSelf);

			$cache_key = $module;

			if (!array_key_exists($cache_key, $cache)) {
				$cacheData = cacheFrontend::getInstance()->loadData('module_perms_' . $owner_id . '_' . $cache_key);

				if (is_array($cacheData)) {
					$cache[$module] = $cacheData;
				} else {
					$sql = "SELECT cp.method, MAX(cp.allow) FROM cms_permissions cp WHERE module = '{$module}' AND {$sql_where} GROUP BY module, method";
					$result = $connection->queryResult($sql);
					$result->setFetchType(IQueryResult::FETCH_ROW);

					$cache[$module] = [];

					foreach ($result as $row) {
						list($cmethod) = $row;
						$cache[$cache_key][] = $cmethod;
					}

					cacheFrontend::getInstance()->saveData('module_perms_' . $owner_id . '_' . $cache_key, $cache[$module], 3600);
				}
			}

			if (in_array($method, $cache[$cache_key]) || in_array(strtolower($method), $cache[$cache_key])) {
				return true;
			}

			return false;
		}

		/**
		 * @todo: отвратительное название для метода, который возвращает массив
		 * Возвращает права пользователя или группы пользователей на страницу
		 * @param int $owner_id id пользователя или группы пользователей
		 * @param int $object_id id страницы, доступ к которой проверяется
		 * @param bool $resetCache игнорировать кеш
		 * @return array(
		 *  0 => bool права на просмотр страницы,
		 *  1 => bool права на редактирование страницы,
		 *  2 => bool права на создание дочерней страницы,
		 *  3 => bool права на удаление страницы,
		 *  4 => bool права на перемещение страницы
		 * )
		 */
		public function isAllowedObject($owner_id, $object_id, $resetCache = false) {
			$object_id = (int) $object_id;
			if ($object_id == 0) {
				return [
						false,
						false,
						false,
						false,
						false
				];
			}

			if ($this->isSv($owner_id)) {
				return [
						true,
						true,
						true,
						true,
						true
				];
			}

			if (array_key_exists($object_id, $this->tempElementPermissions)) {
				$level = $this->tempElementPermissions[$object_id];
				return [
						(bool) ($level & 1),
						(bool) ($level & 2),
						(bool) ($level & 4),
						(bool) ($level & 8),
						(bool) ($level & 16)
				];
			}

			$cache = &$this->elementsCache;

			if (!$resetCache && isset($cache[$object_id]) && isset($cache[$object_id][$owner_id])) {
				return $cache[$object_id][$owner_id];
			}

			$sql_where = $this->makeSqlWhere($owner_id);
			$sql = "SELECT BIT_OR(cp.level) FROM cms3_permissions cp WHERE rel_id = '{$object_id}' AND {$sql_where}";
			$level = false;
			cacheFrontend::getInstance()->loadSql($sql);

			if (!$level || $resetCache) {
				$connection = ConnectionPool::getInstance()->getConnection();
				$result = $connection->queryResult($sql);
				$result->setFetchType(IQueryResult::FETCH_ROW);
				$level = 0;

				if ($result->length() > 0) {
					$fetchResult = $result->fetch();
					$level = (int) array_shift($fetchResult);
				}

				$level = [
						(bool) ($level & 1),
						(bool) ($level & 2),
						(bool) ($level & 4),
						(bool) ($level & 8),
						(bool) ($level & 16)
				];
			}

			if ($level) {
				cacheFrontend::getInstance()->saveSql($sql, $level, 600);
			}

			if (!isset($cache[$object_id])) {
				$cache[$object_id] = [];
			}

			$cache[$object_id][$owner_id] = $level;
			return $level;
		}

		/**
		 * Определяет доступна ли страница для просмотра пользователю или группе
		 * @param int $ownerId идентификатор пользователя или группы
		 * @param int $pageId идентификатор страницы
		 * @return bool
		 */
		public function isPageCanBeViewed($ownerId, $pageId) {
			$ignoreCache = true;
			$permissions = $this->isAllowedObject($ownerId, $pageId, $ignoreCache);

			if (!is_array($permissions) || !isset($permissions[0])) {
				return false;
			}

			return (bool) $permissions[0];
		}

		/**
		 * Узнать, является ли пользователь или группа пользователей $user_id супервайзером
		 * @param bool|int $user_id id пользователя (по умолчанию используется id текущего пользователя)
		 * @return bool true, если пользователь является супервайзером
		 */
		public function isSv($userId = false) {
			static $isSv = [];

			$svGroupId = UmiCms\Service::SystemUsersPermissions()->getSvGroupId();
			$userTypeId = $this->umiTypesHelper->getObjectTypeIdByGuid('users-user');

			if ($userId === false) {
				$userId = UmiCms\Service::Auth()->getUserId();
				$userGroups = $this->umiPropertiesHelper->getPropertyValue($userId, 'groups', $userTypeId);
				if ((is_array($userGroups) && in_array($svGroupId, $userGroups)) || $userId == $svGroupId) {
					return $isSv[$userId] = true;
				}
			}

			if (isset($isSv[$userId])) {
				return $isSv[$userId];
			}

			if (is_null(getRequest('guest-mode')) == false) {
				return $isSv[$userId] = false;
			}

			$userGroups = $this->umiPropertiesHelper->getPropertyValue($userId, 'groups', $userTypeId);

			if ((is_array($userGroups) && in_array($svGroupId, $userGroups)) || $userId == $svGroupId) {
				return $isSv[$userId] = true;
			}

			return $isSv[$userId] = false;
		}

		/**
		 * Узнать, является ли пользователь $user_id администратором, т.е. есть ли у него доступ
		 * к администрированию хотя бы одного модуля
		 * @param Integer|bool $user_id = false id пользователя (по умолчанию используется id текущего пользователя)
		 * @return Boolean true, если пользователь является администратором
		 */
		public function isAdmin($user_id = false, $ignoreCache = false) {
			static $is_admin = [];

			if ($user_id === false) {
				$user_id = UmiCms\Service::Auth()->getUserId();
			}

			if (isset($is_admin[$user_id])) {
				return $is_admin[$user_id];
			}

			if ($this->isSv($user_id)) {
				return $is_admin[$user_id] = true;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql_where = $this->makeSqlWhere($user_id);
			$sql = <<<SQL
	SELECT COUNT(cp.allow)
		FROM cms_permissions cp
		WHERE method IS NULL AND {$sql_where} AND cp.allow IN (1, 2) GROUP BY module
SQL;
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$cnt = 0;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$cnt = (int) array_shift($fetchResult);
			}

			return $is_admin[$user_id] = (bool) $cnt;
		}

		/**
		 * Узнать, является ли пользователь $user_id владельцем объекта (класс umiObject) $object_id
		 * @param Integer $object_id id объекта (класс umiObject)
		 * @param Integer $user_id id пользователя
		 * @return Boolean true, если пользователь является владельцем
		 */
		public function isOwnerOfObject($object_id, $user_id = false) {
			if ($user_id == false) {
				$user_id = UmiCms\Service::Auth()->getUserId();
			}

			if ($user_id == $object_id) {
				return true;
			}

			$object = umiObjectsCollection::getInstance()->getObject($object_id);

			if ($object instanceof umiObject) {
				$owner_id = $object->getOwnerId();
			} else {
				$owner_id = false;
			}

			if ($owner_id === false || $owner_id == $user_id) {
				return true;
			}

			$guestId = UmiCms\Service::SystemUsersPermissions()->getGuestUserId();

			if ($owner_id == $guestId && class_exists('customer')) {
				$customer = customer::get();
				if ($customer && ($customer->id == $owner_id)) {
					return true;
				}
			}

			return false;
		}

		/**
		 * Установить для страницы права по умолчанию
		 * @param int $elementId идентификатор страницы (класс umiHierarchyElement)
		 * @return bool false если произошла ошибка
		 */
		public function setDefaultPermissions($elementId) {
			$umiHierarchy = umiHierarchy::getInstance();
			$umiObjects = umiObjectsCollection::getInstance();
			$umiHierarchyTypes = umiHierarchyTypesCollection::getInstance();

			$elementId = (int) $elementId;

			if (!$umiHierarchy->isExists($elementId)) {
				return false;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$connection->query("START TRANSACTION");
			$sql = "DELETE FROM cms3_permissions WHERE rel_id = '{$elementId}'";
			$connection->query($sql);

			$element = $umiHierarchy->getElement($elementId, true, true);

			if (!$element instanceof iUmiHierarchyElement) {
				return false;
			}

			$sel = new selector('objects');
			$sel->types('object-type')->name('users', 'user');
			$sel->where('groups')->isnull();
			$sel->option('return')->value('id');
			$result = $sel->result();

			$userIds = array_map(function($info) { return (int) $info['id']; }, $result);
			$systemUsersPermissions = UmiCms\Service::SystemUsersPermissions();
			$guestId = (int) $systemUsersPermissions->getGuestUserId();

			if ($guestId) {
				$userIds[] = $guestId;
				$userIds = array_unique($userIds);
			}

			$sel = new selector('objects');
			$sel->types('object-type')->name('users', 'users');
			$sel->option('return')->value('id');
			$result = $sel->result();

			$groupIds = array_map(function($info) { return (int) $info['id']; }, $result);
			$objectIds = array_merge($userIds, $groupIds);

			$ownerId = $element->getObject()->getOwnerId();

			if ($owner = $umiObjects->getObject($ownerId)) {
				if ($ownerGroupIds = $owner->getValue("groups")) {
					$ownerIds = $ownerGroupIds;
				} else {
					$ownerIds = [$ownerId];
				}
			} else {
				$ownerIds = [];
			}

			$hierarchyTypeId = $element->getTypeId();
			$hierarchyType = $umiHierarchyTypes->getType($hierarchyTypeId);

			$module = $hierarchyType->getName();
			$method = $hierarchyType->getExt();

			$svGroupId = $systemUsersPermissions->getSvGroupId();

			foreach ($objectIds as $id) {
				if ($id == $svGroupId) {
					continue;
				}

				if ($module === "content") {
					$method = "page";
				}

				if ($this->isAllowedMethod($id, $module, $method)) {
					if (in_array($id, $ownerIds) || $id == $svGroupId || $this->isAllowedMethod($id, $module, $method . ".edit")) {
						$level =
								self::E_READ_ALLOWED_BIT +
								self::E_EDIT_ALLOWED_BIT +
								self::E_CREATE_ALLOWED_BIT +
								self::E_DELETE_ALLOWED_BIT +
								self::E_MOVE_ALLOWED_BIT;
					} else {
						$level = self::E_READ_ALLOWED_BIT;
					}

					$sql = "INSERT INTO cms3_permissions (rel_id, owner_id, level) VALUES('{$elementId}', '{$id}', '{$level}')";
					$connection->query($sql);
				}
			}

			$connection->query("COMMIT");
			$connection->query("SET AUTOCOMMIT=1");

			$this->cleanupElementPermissions($elementId);

			if (isset($this->elementsCache[$elementId])) {
				unset($this->elementsCache[$elementId]);
			}

			$cacheKey = UmiCms\Service::Auth()->getUserId() . "." . $elementId;
			cacheFrontend::getInstance()->saveSql($cacheKey, [true, true]);

			return true;
		}

		/**
		 * Копирует права с родительского элемента
		 * @param Integer $elementId идентификатор элемента, на который устанавливаем права
		 * @return bool
		 */
		public function setInheritedPermissions($elementId) {
			$elementId = (int) $elementId;
			$hierarchy = umiHierarchy::getInstance();
			$parentId = false;

			if ($element = $hierarchy->getElement($elementId, true)) {
				$parentId = $element->getParentId();
			}

			if (!$parentId) {
				return $this->setDefaultPermissions($elementId);
			}

			$records = $this->getRecordedPermissions($parentId);
			$values = [];

			foreach ($records as $ownerId => $level) {
				$values[] = "('{$elementId}', '{$ownerId}', '{$level}')";
			}

			if (empty($values)) {
				return false;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$connection->query("START TRANSACTION");
			$sql = "DELETE FROM cms3_permissions WHERE rel_id = '{$elementId}'";
			$connection->query($sql);
			$sql = "INSERT INTO cms3_permissions (rel_id, owner_id, level) VALUES " . implode(", ", $values);
			$connection->query($sql);
			$connection->query("COMMIT");
			$connection->query("SET AUTOCOMMIT=1");
			$this->isAllowedObject(UmiCms\Service::Auth()->getUserId(), $elementId, true);
			return true;
		}

		/**
		 * Удалить все права на странциу $elementId для ползователя или группы $ownerId
		 * @param Integer $elementId id страницы (класс umiHierarchyElement)
		 * @param Integer $ownerId =false id пользователя или группы, чьи права сбрасываются. Если false, то права
		 *     сбрасываются для всех пользователей
		 * @return bool
		 */
		public function resetElementPermissions($elementId, $ownerId = false) {
			$elementId = (int) $elementId;

			if ($ownerId === false) {
				$sql = "DELETE FROM cms3_permissions WHERE rel_id = '{$elementId}'";

				if (isset($this->elementsCache[$elementId])) {
					unset($this->elementsCache[$elementId]);
				}
			} else {
				$ownerId = (int) $ownerId;
				$sql = "DELETE FROM cms3_permissions WHERE owner_id = '{$ownerId}' AND rel_id = '{$elementId}'";

				if (isset($this->elementsCache[$elementId]) && isset($this->elementsCache[$elementId][$ownerId])) {
					unset($this->elementsCache[$elementId][$ownerId]);
				}
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$connection->query($sql);
			return true;
		}

		/**
		 * Удалить права на все страницы для пользователя или группы
		 * @param int $ownerId ид пользователя или группы
		 * @return void
		 */
		public function deleteElementsPermissionsByOwnerId($ownerId) {
			$ownerId = (int) $ownerId;
			$connection = ConnectionPool::getInstance()->getConnection();
			$deletion = "DELETE FROM `cms3_permissions` WHERE `owner_id` = $ownerId";
			$connection->query($deletion);
		}

		/**
		 * Сбросить все права на модули и методы для пользователя или группы $ownerId
		 * @param int $ownerId id пользователя или группы пользователей
		 * @param array $modules = null массив, который указывает модули, для которых сбросить права.
		 *   По умолчанию, сбрасываются права на все модули.
		 * @return bool
		 */
		public function resetModulesPermissions($ownerId, $modules = null) {
			$connection = ConnectionPool::getInstance()->getConnection();

			$ownerId = (int) $ownerId;
			$sql = "DELETE FROM cms_permissions WHERE owner_id = '{$ownerId}'";

			if (is_array($modules) && count($modules) > 0) {
				$modules = array_map([$connection, 'escape'], $modules);
				$sql = "DELETE FROM cms_permissions WHERE owner_id = '{$ownerId}' AND module IN ('" . implode("', '", $modules) . "')";
			}

			$connection->query($sql);

			if (!is_array($modules)) {
				$modules = cmsController::getInstance()->getModulesList();
			}

			$cacheFrontend = cacheFrontend::getInstance();

			foreach ($modules as $module) {
				$cacheFrontend->deleteKey('module_perms_' . $ownerId . '_' . $module, true);
			}

			return true;
		}

		/**
		 * Установить определенные права на страница $elementId для пользователя или группы $ownerId
		 * @param Integer $ownerId id пользователя или группы пользователей
		 * @param Integer $elementId id страницы (класс umiHierarchyElement), для которой меняются права
		 * @param Integer $level уровень выставляемых прав от "0" до "31":
		 * ---------------------------------------------------------------------------
		 * | значение | чтение | редактирование | создание | удаление |  перемещение |
		 * |    0     |   -    |       -        |    -     |    -     |       -      |
		 * |    1     |   +    |       -        |    -     |    -     |       -      |
		 * |    3     |   +    |       +        |    -     |    -     |       -      |
		 * |    7     |   +    |       +        |    +     |    -     |       -      |
		 * |    15    |   +    |       +        |    +     |    +     |       -      |
		 * |    31    |   +    |       +        |    +     |    +     |       +      |
		 * ---------------------------------------------------------------------------
		 * @return Boolean true если не произошло ошибки
		 */
		public function setElementPermissions($ownerId, $elementId, $level) {
			$ownerId = (int) $ownerId;
			$elementId = (int) $elementId;
			$level = (int) $level;

			if ($elementId == 0 || $ownerId == 0) {
				return false;
			}

			if (isset($this->elementsCache[$elementId]) && isset($this->elementsCache[$elementId][$ownerId])) {
				unset($this->elementsCache[$elementId][$ownerId]);
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql_reset = "DELETE FROM cms3_permissions WHERE owner_id = '" . $ownerId . "' AND rel_id = '" . $elementId . "'";
			$connection->query($sql_reset);

			$sql = "INSERT INTO cms3_permissions (owner_id, rel_id, level) VALUES('{$ownerId}', '{$elementId}', '{$level}')";
			$connection->query($sql);

			$this->cleanupElementPermissions($elementId);
			$this->isAllowedObject($ownerId, $elementId, true);

			return true;
		}

		/**
		 * Разрешить пользователю или группе $owner_id права на $module/$method
		 * @param Integer $ownerId id пользователя или группы пользователей
		 * @param String $module название модуля
		 * @param String|bool $method =false название метода
		 * @return bool
		 */
		public function setModulesPermissions($ownerId, $module, $method = false, $cleanupPermissions = true) {
			$ownerId = (int) $ownerId;
			$connection = ConnectionPool::getInstance()->getConnection();
			$module = $connection->escape($module);

			if ($method !== false) {
				return $this->setMethodPermissions($ownerId, $module, $method, $cleanupPermissions);
			}

			$sql = "INSERT INTO cms_permissions (owner_id, module, method, allow) VALUES('{$ownerId}', '{$module}', NULL, '1')";
			$connection->query($sql);

			if ($cleanupPermissions) {
				$this->cleanupBasePermissions();
			}

			return true;
		}

		protected function setMethodPermissions($ownerId, $module, $method, $cleanupPermissions = true) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$method = $connection->escape($method);
			$module = $connection->escape($module);
			$ownerId = (int) $ownerId;

			$sql = "INSERT INTO cms_permissions (owner_id, module, method, allow) VALUES('{$ownerId}', '{$module}', '{$method}', '1')";
			$connection->query($sql);

			$this->methodsPermissions[$ownerId][$module][] = $method;

			if ($cleanupPermissions) {
				$this->cleanupBasePermissions();
			}

			return true;
		}

		/**
		 * Узнать, имеет ли пользователь или группа в принципе права на какие-нибудь страницы
		 * @param Integer $ownerId id пользователя или группы
		 * @return Boolean false, если записей нет
		 */
		public function hasUserPermissions($ownerId) {
			$ownerId = (int) $ownerId;
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT COUNT(*) FROM cms3_permissions WHERE owner_id = '{$ownerId}'";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$count = 0;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$count = (int) array_shift($fetchResult);
			}

			return (bool) $count;
		}

		/**
		 * Узнать, имеет ли пользователь или группа права на какие-нибудь модули
		 * @param int $ownerId id пользователя или группы
		 * @return bool
		 */
		public function hasUserModulesPermissions($ownerId) {
			$ownerId = intval($ownerId);

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT `module` FROM `cms_permissions` WHERE `owner_id` = $ownerId AND `allow` = 1 LIMIT 0,1";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ASSOC);

			return ($result->length() == 1);
		}

		/**
		 * Скопировать права на все страницы из $fromUserId в $toUserId
		 * @param Integer $fromUserId id пользователя или группы пользователей, из которых копируются права
		 * @param Integer $fromUserId id пользователя или группы пользователей, в которые копируются права
		 * @return bool
		 */
		public function copyHierarchyPermissions($fromUserId, $toUserId) {

			if ($fromUserId == UmiCms\Service::SystemUsersPermissions()->getGuestUserId()) {
				return false;    //No need in cloning guest permissions now
			}

			$fromUserId = (int) $fromUserId;
			$toUserId = (int) $toUserId;
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "INSERT INTO cms3_permissions (level, rel_id, owner_id) SELECT level, rel_id, '{$toUserId}' FROM cms3_permissions WHERE owner_id = '{$fromUserId}'";
			$connection->query($sql);

			return true;
		}

		/**
		 * Системный метод. Получить массив прав из permissions.php и permissions.custom.php
		 * @return array
		 */
		public function getStaticPermissions($module, $templater = false) {
			static $cache = [];

			if (isset($cache[$module]) && !$templater) {
				return $cache[$module];
			}

			$static_file = SYS_MODULES_PATH . $module . "/permissions.php";
			if (file_exists($static_file)) {
				require $static_file;
				if (isset($permissions)) {
					$static_permissions = $permissions;

					$static_file_custom = SYS_MODULES_PATH . $module . "/permissions.custom.php";
					if (file_exists($static_file_custom)) {
						unset($permissions);
						require $static_file_custom;
						if (isset($permissions)) {
							$static_permissions = array_merge_recursive($static_permissions, $permissions);
						}
					}

					// подключаем права из ресурсов шаблона
					// TODO: refactoring
					if ($resourcesDir = cmsController::getInstance()->getResourcesDirectory()) {
						$static_file_custom = $resourcesDir . '/classes/modules/' . $module . "/permissions.php";
						if (file_exists($static_file_custom)) {
							unset($permissions);
							require $static_file_custom;
							if (isset($permissions)) {
								$static_permissions = array_merge_recursive($static_permissions, $permissions);
							}
						}
					}

					// подключение прав из расширений
					$path = SYS_MODULES_PATH . $module . "/ext/permissions.*.php";

					$fileNames = glob($path);
					if (is_array($fileNames)) {
						foreach ($fileNames as $filename) {
							if (file_exists($filename)) {
								unset($permissions);
								require $filename;
								if (isset($permissions)) {
									$static_permissions = array_merge_recursive($static_permissions, $permissions);
								}
							}
						}
					}

					$cache[$module] = $static_permissions;
					unset($static_permissions);
					unset($permissions);
				} else {
					$cache[$module] = [];
				}
			} else {
				$cache[$module] = [];
			}

			return $cache[$module];
		}

		/**
		 * Получить название корневого метода в системе приритета прав для $module::$method
		 * @param String $module название модуля
		 * @param String $method название метода
		 * @return String название корневого метода
		 */
		protected function getBaseMethodName($module, $method) {
			$methods = $this->getStaticPermissions($module);

			if ($method && is_array($methods)) {

				if (array_key_exists($method, $methods)) {
					return $method;
				}

				foreach ($methods as $base_method => $sub_methods) {
					if (is_array($sub_methods)) {
						if (in_array($method, $sub_methods) || in_array(strtolower($method), $sub_methods)) {
							return $base_method;
						}
					}
				}

				return $method;
			}

			return $method;
		}

		/**
		 * Удалить все записи о правах на модули и методы для пользователей, если они ниже, чем у гостя
		 */
		public function cleanupBasePermissions() {
			$guestId = UmiCms\Service::SystemUsersPermissions()->getGuestUserId();

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT module, method FROM cms_permissions WHERE owner_id = '{$guestId}' AND allow = 1";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			$sql = [];

			foreach ($result as $row) {
				list($module, $method) = $row;
				$sql[] = ($method) ? "(module = '{$module}' AND method = '{$method}')" : "(module = '{$module}' AND method IS NULL)";
			}

			if (!empty($sql)) {
				$connection->query("DELETE FROM cms_permissions WHERE owner_id != '{$guestId}' AND (" . implode(' OR ', $sql) . ")");
			}
		}

		/**
		 * Удалить для страницы  с id $rel_id записи о правах пользователей, которые ниже, чем у гостя
		 * @param Integer $rel_id id страница (класс umiHierarchyElement)
		 */
		protected function cleanupElementPermissions($rel_id) {
			$rel_id = (int) $rel_id;
			$guestId = UmiCms\Service::SystemUsersPermissions()->getGuestUserId();

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT level FROM cms3_permissions WHERE owner_id = '{$guestId}' AND rel_id = {$rel_id}";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			$maxLevel = 0;

			foreach ($result as $row) {
				$level = array_shift($row);

				if ($level > $maxLevel) {
					$maxLevel = $level;
				}
			}

			$connection->query("DELETE FROM cms3_permissions WHERE owner_id != '{$guestId}' AND level <= {$maxLevel} AND rel_id = {$rel_id}");
		}

		/**
		 * Узнать, разрешено ли пользователю или группе $owner_id администрировать домен $domain_id
		 * @param Integer $owner_id id пользователя или группы пользователей
		 * @param Integer $domain_id id домена (класс domain)
		 * @return Integer 1, если доступ разрешен, 0 если нет
		 */
		public function isAllowedDomain($owner_id, $domain_id) {
			$owner_id = (int) $owner_id;
			$domain_id = (int) $domain_id;

			if ($this->isSv($owner_id)) {
				return 1;
			}

			$sql_where_owners = $this->makeSqlWhere($owner_id);
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT MAX(cp.allow) FROM cms_permissions cp WHERE cp.module = 'domain' AND cp.method = '{$domain_id}' AND " . $sql_where_owners;
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$isAllowed = 0;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$isAllowed = (int) array_shift($fetchResult);
			}

			return $isAllowed;
		}

		/**
		 * Установить права пользователю или группе $owner_id на администрирование домена $domain_id
		 * @param Integer $owner_id id пользователя или группы пользователей
		 * @param Integer $domain_id id домена (класс domain)
		 * @param Boolean $allow =true если true, то доступ разрешен
		 * @return bool
		 */
		public function setAllowedDomain($owner_id, $domain_id, $allow = 1) {
			$owner_id = (int) $owner_id;
			$domain_id = (int) $domain_id;
			$allow = (int) $allow;

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "DELETE FROM cms_permissions WHERE module = 'domain' AND method = '{$domain_id}' AND owner_id = '{$owner_id}'";
			$connection->query($sql);

			$sql = "INSERT INTO cms_permissions (module, method, owner_id, allow) VALUES('domain', '{$domain_id}', '{$owner_id}', '{$allow}')";
			$connection->query($sql);

			return true;
		}

		/**
		 * Установить права по умолчанию для страницы $element по отношению к пользователю $owner_id
		 * @param umiHierarchyElement $element экземпляр страницы
		 * @param Integer $owner_id id пользователя или группы пользователей
		 * @return Integer уровен доступа к странице, который был выбран системой
		 */
		public function setDefaultElementPermissions(iUmiHierarchyElement $element, $owner_id) {
			$module = $element->getModule();
			$method = $element->getMethod();

			$level = 0;
			if ($this->isAllowedMethod($owner_id, $module, $method, true)) {
				$level = self::E_READ_ALLOWED_BIT;
			}

			if ($this->isAllowedMethod($owner_id, $module, $method . ".edit", true)) {
				$level =
						self::E_READ_ALLOWED_BIT +
						self::E_EDIT_ALLOWED_BIT +
						self::E_CREATE_ALLOWED_BIT +
						self::E_DELETE_ALLOWED_BIT +
						self::E_MOVE_ALLOWED_BIT;
			}

			$this->setElementPermissions($owner_id, $element->getId(), $level);

			return $level;
		}

		/**
		 * Сбросить для пользователя или группы $owner_id права на все страницы на дефолтные
		 * @param Integer $owner_id id пользователя или группы пользователей
		 */
		public function setAllElementsDefaultPermissions($owner_id) {
			$owner_id = (int) $owner_id;
			$hierarchyTypes = umiHierarchyTypesCollection::getInstance();

			$this->elementsCache = [];

			$owner = $this->getOwnerType($owner_id);
			if (is_numeric($owner)) {
				$owner = [];
			}

			$owner[] = UmiCms\Service::SystemUsersPermissions()->getGuestUserId();
			$owner = array_unique($owner);

			$connection = ConnectionPool::getInstance()->getConnection();
			$connection->query("START TRANSACTION");

			$read = [];
			$write = [];

			/**
			 * @var iUmiHierarchyType $hierarchyType
			 */
			foreach ($hierarchyTypes->getTypesList() as $hierarchyType) {
				$module = $hierarchyType->getName();
				$method = $hierarchyType->getExt();

				if ($this->isAllowedMethod($owner_id, $module, $method . ".edit", true)) {
					foreach ($owner as $gid) {
						if ($gid == $owner_id) {
							continue;
						}

						if ($this->isAllowedMethod($gid, $module, $method . ".edit", true)) {
							continue 2;
						}
					}
					$write[] = $hierarchyType->getId();
				} else {
					if ($this->isAllowedMethod($owner_id, $module, $method, true)) {
						foreach ($owner as $gid) {
							if ($gid == $owner_id) {
								continue;
							}

							if ($this->isAllowedMethod($gid, $module, $method, true)) {
								continue 2;
							}
						}

						$read[] = $hierarchyType->getId();
					}
				}
			}

			if (sizeof($read)) {
				$types = implode(", ", $read);

				$sql = <<<SQL
	INSERT INTO cms3_permissions (level, owner_id, rel_id)
		SELECT 1, '{$owner_id}', id FROM cms3_hierarchy WHERE type_id IN ({$types})
SQL;
				$connection->query($sql);
			}

			if (sizeof($write)) {
				$types = implode(", ", $write);

				$sql = <<<SQL
	INSERT INTO cms3_permissions (level, owner_id, rel_id)
		SELECT 31, '{$owner_id}', id FROM cms3_hierarchy WHERE type_id IN ({$types})
SQL;
				$connection->query($sql);
			}

			$connection->query("COMMIT");
		}

		/**
		 * Получить список всех пользователей или групп, имеющих права на страницу $elementId
		 * @param Integer $elementId id страницы
		 * @param Integer $level = 1 искомый уровень прав
		 * @return array массив id пользователей или групп, имеющих права на страницу
		 */
		public function getUsersByElementPermissions($elementId, $level = 1) {
			$elementId = (int) $elementId;
			$level = (int) $level;

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT owner_id FROM cms3_permissions WHERE rel_id = '{$elementId}' AND level >= '{$level}'";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			$owners = [];

			foreach ($result as $row) {
				$owners[] = (int) array_shift($row);
			}

			return $owners;
		}

		/**
		 * Получить список сохраненных прав для страницы $elementId
		 * @param Integer $elementId
		 * @return array $ownerId => $permissionsLevel
		 */
		public function getRecordedPermissions($elementId) {
			$elementId = (int) $elementId;

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT owner_id, level FROM cms3_permissions WHERE rel_id = '{$elementId}'";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			$records = [];

			foreach ($result as $row) {
				list($ownerId, $level) = $row;
				$records[$ownerId] = (int) $level;
			}

			return $records;
		}

		/**
		 * Указать права на страницу. Влияет только на текущую сессию, данные в базе изменены не будут
		 * @param Integer $elementId id страницы
		 * @param Integer $level = 1 уровень прав доступа (0-3).
		 */
		public function pushElementPermissions($elementId, $level = 1) {
			$this->tempElementPermissions[$elementId] = (int) $level;
		}

		/**
		 * @deprecated
		 * Узнать, авторизован ли текущий пользователь
		 * @return Boolean true, если авторизован
		 */
		public function isAuth() {
			return !UmiCms\Service::Auth()->isLoginAsGuest();
		}

		/**
		 * @deprecated
		 * Алиас для isAuth()
		 * @return bool
		 */
		public function is_auth() {
			return $this->isAuth();
		}

		public function getPrivileged($perms) {
			if (!sizeof($perms)) {
				return [];
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = 'SELECT owner_id FROM cms_permissions WHERE ';
			$sqls = [];

			foreach ($perms as $perm) {
				$module = $connection->escape(getArrayKey($perm, 0));
				$method = $connection->escape($this->getBaseMethodName($module, getArrayKey($perm, 1)));
				$sqls[] = "(module = '{$module}' AND method = '{$method}')";
			}

			$sql .= implode(' OR ', $sqls);
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			$owners = [];

			foreach ($result as $row) {
				$owners[] = array_shift($row);
			}

			$owners = array_unique($owners);
			return $owners;
		}

		/**
		 * Очищает внутренний кеш класса
		 * @return void
		 */
		public function clearCache() {
			$this->elementsCache = [];
			$this->tempElementPermissions = [];
			$this->methodsPermissions = [];
		}

		/**
		 * @deprecated
		 */
		public function loadReadablePages() {
			return true;
		}

		/**
		 * @deprecated
		 */
		public function isReadablePagesLoaded() {
			return false;
		}

		/**
		 * @deprecated
		 */
		public function getReadablePagesIds() {
			return [];
		}

		/**
		 * @deprecated
		 */
		public function isPageReadable($pageId, $resetCache = false) {
			return $this->isAllowedObject(UmiCms\Service::Auth()->getUserId(), $pageId, $resetCache);
		}

		/**
		 * @deprecated
		 */
		public function hashPassword($password, $algorithm = self::SHA256) {
			return UmiCms\Service::PasswordHashAlgorithm()->hash($password, $algorithm);
		}

		/**
		 * @deprecated
		 */
		public function isPasswordHashedWithMd5($hashedPassword, $rawPassword) {
			return UmiCms\Service::PasswordHashAlgorithm()->isHashedWithMd5($hashedPassword, $rawPassword);
		}

		/**
		 * @deprecated
		 */
		public function getSvUserId() {
			return UmiCms\Service::SystemUsersPermissions()->getSvUserId();
		}

		/**
		 * @deprecated
		 */
		public function getSvGroupId() {
			return UmiCms\Service::SystemUsersPermissions()->getSvGroupId();
		}

		/**
		 * @deprecated
		 */
		public function getGuestUserId() {
			return UmiCms\Service::SystemUsersPermissions()->getGuestUserId();
		}

		/**
		 * @deprecated
		 */
		public static function getGuestId() {
			return UmiCms\Service::SystemUsersPermissions()->getGuestUserId();
		}

		/**
		 * @deprecated
		 */
		public function checkLogin($login, $password) {
			$userId = UmiCms\Service::Auth()->checkLogin($login, $password);
			return selector::get('object')->id($userId);
		}

		/**
		 * @deprecated
		 */
		public function getUserId() {
			return UmiCms\Service::Auth()->getUserId();
		}

		/**
		 * @deprecated
		 */
		public function loginAsUser($userId) {
			return UmiCms\Service::Auth()->loginUsingId($userId);
		}

		protected function isAdminAllowedMethod($module, $method) {
			$methods = [
					'content' => [
						'json_mini_browser',
						'old_json_load_files',
						'json_load_files',
						'json_load_zip_folder',
						'load_tree_node',
						'get_editable_region',
						'save_editable_region',
						'widget_create',
						'widget_delete',
						'getObjectsByTypeList',
						'getObjectsByBaseTypeList',
						'json_get_images_panel',
						'json_create_imanager_object',
						'domainTemplates',
						'json_unlock_page',
						'tree_unlock_page'
					],
					'backup' => [
						'backup_panel'
					],
					'data' => [
						'guide_items',
						'guide_items_all',
						'json_load_hierarchy_level'
					],
					'webo' => [
						'show'
					],
					'users' => [
						'getFavourites',
						'json_change_dock',
						'saveUserSettings',
						'loadUserSettings'
					],
					'*' => [
						'dataset_config'
					]
			];

			if (isset($methods[$module])) {
				if (in_array($method, $methods[$module])) {
					return true;
				}
			}

			if (isset($methods['*'])) {
				if (in_array($method, $methods['*'])) {
					return true;
				}
			}

			return false;
		}
	}
